﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4
{
    class Program
    {
        static int[] numberFrequencies = new int[11];
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the frequency distribution bar chart app.\n Please enter any number of values between 0 and 10.\n Please hit enter to stop entering numbers, we will display a frequency distribution bar chart.");

            EnterNumbers();

            DisplayFrequencyDistributionBarChart();

            Console.ReadKey();
        }

        private static void DisplayFrequencyDistributionBarChart()
        {
            for (int i = 0; i < numberFrequencies.Length; i++)
            {
                string line = i + "\t";
                for (int j = 0; j < numberFrequencies[i]; j++)
                {
                    line += "*";
                }
                Console.WriteLine(line);
            }
        }

        private static void EnterNumbers()
        {
            string input = string.Empty;
            do
            {
                input = EnterNumber();
            }
            while (!string.IsNullOrEmpty(input));
        }

        private static string EnterNumber()
        {
            Console.WriteLine("Please enter a number between 0 and 10");
            var input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                if (int.TryParse(input, out int result))
                {
                    if (result< 0 || result>10)
                    {
                        Console.WriteLine("a value outside the acceptable range is entered");
                        return EnterNumber();
                    }

                    numberFrequencies[result]++;
                }
                else
                {
                    Console.WriteLine("a non-numeric character is entered");
                    return EnterNumber();
                }
            }

            return input;
        }
    }
}
