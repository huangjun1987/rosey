﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArraysApp
{
    public partial class Form1 : Form
    {
        double[] array1 = new double[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        double[] array2 = new double[11] { 22, 32, 14, 54, 23, 71, 234, 63, 91, 11, 12 };

        public Form1()
        {
            InitializeComponent();

            double[] array3 = CalculateArray3(array1, array2);
            DisplayResults(array1, array2, array3);            
        }

        private double[] CalculateArray3(double[] array1, double[] array2)
        {
            int array3Length = Math.Max(array1.Length, array2.Length);
            double[] array3 = new double[array3Length];

            for (int i = 0; i < array3Length; i++)
            {
                array3[i] = GetValueOr1(array1, i) * GetValueOr1(array2, i);
            }

            return array3;
        }

        private void DisplayResults(double[] array1, double[] array2, double[] array3)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Array1 * Array2 = Array3");
            sb.AppendLine();
            sb.AppendLine("Array1\tArray2\tArray3");

            for (int i = 0; i < array3.Length; i++)
            {
                sb.AppendLine($"{GetValueOr1(array1, i)}\t{GetValueOr1(array2, i)}\t{array3[i]}");
            }

            MessageBox.Show(sb.ToString());
        }

        private double GetValueOr1(double[] array, int i)
        {
            if (i < array.Length)
            {
                return array[i];
            }

            return 1;
        }
    }
}
