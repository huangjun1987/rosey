﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4
{
    public class Exercise
    {
        public string Name { get; internal set; }
        public double Price { get; internal set; }
        public string[] TimeSlots { get; internal set; }
        public int[,] Attendance { get; internal set; }

        public double GetDailyRevenue(int day)
        {
            int dailyHours = GetDailyHours(day);
            return dailyHours * Price;
        }
        public double GetWeeklyRevenue()
        {
            double weeklyRevenue = 0;
            for (int i = 0; i < 6; i++)
            {
                weeklyRevenue += GetDailyRevenue(i);
            }
            return weeklyRevenue;
        }
        private int GetDailyHours(int day)
        {
            int hours = 0;
            for (int i = 0; i < TimeSlots.Length; i++)
            {
                hours += Attendance[day, i];
            }
            return hours;
        }
    }
}
