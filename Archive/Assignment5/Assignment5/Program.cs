﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4
{
    class Program
    {
        static string[] dayOfWeeks = new string[6] {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday" };
        static void Main(string[] args)
        {
            char inner = 'a';
            string str = "SS";
            double d = 10.00;
            switch (d)
            {
                case 10.22:
                default:
                    break;
            }
            int count = 0;
            for (int i = 0; i < 3; i++)
            {

            }
            Console.WriteLine(5>57%8); 

            Console.WriteLine("Welcome to the Tappan Gym app.\n We will display the number of attendees per time slot for each type of excercise. \nWe will also column show the revenue generated each day and the overall revenue per type of exercise. ");

            Exercise zumba = new Exercise();
            zumba.Name = "Zumba";
            zumba.Price = 4.00;
            zumba.TimeSlots = new string[4] { "1:00", "3:00", "5:00", "7:00" };
            zumba.Attendance = new int[6,4]
            {
                { 12,    10,  17,  22 },
                {11, 13,  17,  22 },
                { 12,    10,  22,  22 },           
                { 9, 14,  17,  22},
                 { 12,    10,  21,  12},
                { 12,    10,  5,   10 }
            };
            DisplayExercise(zumba);
            Console.WriteLine();
            Exercise spinning = new Exercise();
            spinning.Name = "Spinning";
            spinning.Price = 5.00;
            spinning.TimeSlots = new string[4] { "2:00", "4:00", "6:00", "8:00" };
            spinning.Attendance = new int[6, 4]
            {
                { 7,    11,  15,  8 },
                {9, 9,  9,  9 },
                { 3,    12,  13,  7 },
                { 2, 9,  9,  10},
                 { 8,    4,  9,  4},
                { 4,    5,  9,   3 }
            };
            DisplayExercise(spinning);

            Console.ReadKey();
        }

        private static void DisplayExercise(Exercise exercise)
        {
            Console.WriteLine(exercise.Name.PadLeft(30, ' '));
            string header = "".PadRight(15, ' ') + "\t";
            for (int i = 0; i < exercise.TimeSlots.Length; i++)
            {
                header += exercise.TimeSlots[i] + "\t";
            }
            header += "Daily Revenue";
            Console.WriteLine(header);

            for (int i = 0; i < dayOfWeeks.Length; i++)
            {
                string line = dayOfWeeks[i].PadRight(15,' ') + "\t";
                for (int j = 0; j < exercise.TimeSlots.Length; j++)
                {
                    line += exercise.Attendance[i, j] + "\t";
                }
                line += exercise.GetDailyRevenue(i).ToString("c2");
                Console.WriteLine(line);
            }
            Console.WriteLine("Total Revenue: " + exercise.GetWeeklyRevenue().ToString("c2"));

        }


    }
}
